package com.example.edwinavila.timefighter

import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.LiveFolders.INTENT
import android.text.InputType
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_game_main.*
import java.util.*

class GameMainActivity : AppCompatActivity(){


    val database = FirebaseDatabase.getInstance();
    val ref = database.getReference("todo-list");


    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var tapMeButton: Button
    internal var score= 0
    internal var gameStarted = false
    internal  lateinit var  countDownTimer: CountDownTimer
    internal var countDownInterval = 1000L
    internal var initialCountDown = 10000L
    internal var timeLeft = 5

    internal val TAG = GameMainActivity::class.java.simpleName

    companion object {
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        val INTENT_LIST_ID = "listID";
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_main)



        Log.d(TAG, "onCreate called. Score is $score")

        //connect views to variables
        gameScoreTextView = findViewById(R.id.game_score_text_view)
        timeLeftTextView = findViewById(R.id.time_left_text_view)
        tapMeButton = findViewById<Button>(R.id.tap_me_button)
        tapMeButton.setOnClickListener { _ -> incrementScore() } //para decir que no es nada,
                                                                // _ se pasa una variable en donde esta el guión: referencia del botón

        if(savedInstanceState != null){
            //vamos a sacar del metodo savedInstanceState, para aginar a las variables de la clase
            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else{
            resetGame()
        }

        tapMeButton.setOnClickListener{_ -> incrementScore()}

        fab.setOnClickListener{
            goScreenGame();
        }

    }



    private fun resetGame(){
        score = 0
        timeLeft = 10

        val gameScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = gameScore

        val timeLeftText = getString(R.string.time_left, Integer.toString(timeLeft))
        timeLeftTextView.text = timeLeftText


        countDownTimer = object : CountDownTimer(initialCountDown,countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                //cada vez que se ejecute te va a salir lo que te falta para llegar a 0
                timeLeft = millisUntilFinished.toInt() / 1000
                //en
                timeLeftTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
                //para imprimir el valor en el text view
            }
            override fun onFinish() {
             endGame()
            }
        }

        gameStarted = false

    }


    private fun incrementScore(){
        score ++
        //val newScore = "Your score: " + Integer.toString(score)
        val newScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = newScore

        if (!gameStarted){
            startGame()
        }
    }

    private fun startGame(){
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.game_over_message, Integer.toString(score)),Toast.LENGTH_LONG).show()
        val scoreSaved = score
        showCreateListDialog(scoreSaved)
        resetGame()
    }

    private fun restoreGame(){
        val restoredScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = restoredScore

        val restoredTime = getString(R.string.time_left, Integer.toString(timeLeft))
        timeLeftTextView.text = restoredTime

        countDownTimer = object : CountDownTimer(timeLeft*1000L, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left, timeLeft.toString())
            }

            override fun onFinish() {
             endGame()
            }
        }
        countDownTimer.start()
        gameStarted = true
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(SCORE_KEY,score)//guardamos el estado
        outState.putInt(TIME_LEFT_KEY,timeLeft)
        countDownTimer.cancel()
        Log.d(TAG,"onSaveInstanceState: score = $score & timeleft = $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy called")

    }


    private fun showCreateListDialog(scoreBDD : Int){
        //take the strings variables
        val dialogTitle = getString(R.string.name_of_list)
        val positiveButtonTitle = getString(R.string.create_list)
        val builder = AlertDialog.Builder(this)
        val listTitleEditText = EditText(this)
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT
        builder.setTitle(dialogTitle)
        builder.setView(listTitleEditText)
        builder.setPositiveButton(positiveButtonTitle){dialog, i ->
            val newList = listTitleEditText.text.toString()
            ref.child(UUID.randomUUID().toString()).child("List").setValue(newList+" Score: "+scoreBDD)
            dialog.dismiss()


        }

        builder.create().show()
    }


    private fun goScreenGame(){
        val intentGoGame = Intent(this,activity_score_game::class.java )
        startActivity(intentGoGame);
    }



}
