package com.example.edwinavila.timefighter

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main__presentation.*


class MainActivity_Presentation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main__presentation)

        buttonEnterGame.setOnClickListener{
            goScreenGame()
        }

    }


    fun goScreenGame(){
        val intentGoGame = Intent(this,GameMainActivity::class.java )
        this.startActivity(intentGoGame)
    }
}
