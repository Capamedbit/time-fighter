package com.example.edwinavila.timefighter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase

class activity_score_game: AppCompatActivity() {


    lateinit var listRecyclerView: RecyclerView

    val database = FirebaseDatabase.getInstance();
    val ref = database.getReference("todo-list");


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score_game)

        listRecyclerView = findViewById(R.id.list_recycler_view)
        //vamos a utilizar en linea ....vetical ver horizaonal
        listRecyclerView.layoutManager = LinearLayoutManager(this)

        listRecyclerView.adapter = ListSelectionRecyclerViewAdapter(ref)

    }
}
